Pod::Spec.new do |s|
  s.name         = "ODMembership"
  s.version      = "1.0.6"
  s.summary      = "a framework to apply for The Old's free association penang."
  s.description  = "ODMembership is a base framework for membership logic. Any application that requires ob-boarding and membership feature can utilize this framework to create user experience."
  s.homepage     = "http://www.opendynamics.com.my"
  s.license      = "GNU GPLv3"
  s.author       = { "Farooq Zaman" => "farooq@opendynamics.com.my" }
  s.platform     = :ios, "10.0"
  s.source       = { :git => "https://farooq_zaman@bitbucket.org/farooq_zaman/odmembership.git", :tag => "1.0.6", :branch => "mma"}
  s.source_files = "ODMembership", "ODMembership/**/*.{h,m,swift}"
  s.resources    = "ODMembership/**/*.{xcassets,storyboard,xib}"

  s.dependency 'DZNPhotoPickerController/Editor'
  s.dependency 'libPhoneNumber-iOS', '~> 0.8'
  s.dependency 'KVNProgress'
  s.dependency 'Alamofire', '~> 4.0'
  s.dependency 'AlamofireObjectMapper', '~> 4.0'
  s.dependency 'AlamofireImage', '~> 3.1'
  s.dependency 'RealmSwift'
  s.dependency 'ODJoget', '~> 0.2.2'
  s.dependency 'ODControls', '~> 1.2.6'

  s.pod_target_xcconfig = { 'SWIFT_VERSION' => '3' }
end
