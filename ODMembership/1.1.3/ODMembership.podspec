Pod::Spec.new do |s|
  s.name         = "ODMembership"
  s.version      = "1.1.3"
  s.summary      = "a framework to apply for The Old's free association penang."
  s.description  = "ODMembership is a base framework for membership logic. Any application that requires ob-boarding and membership feature can utilize this framework to create user experience."
  s.homepage     = "http://www.opendynamics.com.my"
  s.license      = "GNU GPLv3"
  s.author       = { "Farooq Zaman" => "farooq@opendynamics.com.my" }
  s.platform     = :ios, "10.0"
  s.source       = { :git => "https://farooq_zaman@bitbucket.org/opendynamics/odmembership.git", :tag => "1.1.3", :branch => "mma"}
  s.source_files = "ODMembership", "ODMembership/**/*.{h,m,swift}"
  s.resources    = "ODMembership/**/*.{xcassets,storyboard,xib}"

  s.dependency 'DZNPhotoPickerController/Editor'
  s.dependency 'libPhoneNumber-iOS', '~> 0.9.2'
  s.dependency 'KVNProgress'
  s.dependency 'Alamofire', '~> 4.5'
  s.dependency 'AlamofireObjectMapper', '~> 5.0'
  s.dependency 'AlamofireImage', '~> 3.3'
  s.dependency 'Realm', '~> 3.0.0'
  s.dependency 'RealmSwift', '~> 3.0.0'
  s.dependency 'ODJoget', '~> 0.2.9'
  s.dependency 'ODControls', '~> 2.0.1'

  s.pod_target_xcconfig = { 'SWIFT_VERSION' => '3.2' }
end
