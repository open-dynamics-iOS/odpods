#
# Be sure to run `pod lib lint ODControls.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
    s.name             = 'ODControls'
    s.version          = '2.0.0'
    s.summary          = 'A collection of iOS controls in swift to facilitate creation of forms in iOS'
    s.description      = '1. PickerView text field, 2. DatePicker text field, 3. Multiple Selection textfield'
    s.homepage         = 'http://www.opendynamics.com.my'
    s.license          = 'GNU GPLv3'
    s.author           = { "Farooq Zaman" => "farooq@opendynamics.com.my" }
    s.platform         = :ios, "10.0"
    s.source           = { :git => 'https://bitbucket.org/open-dynamics-iOS/custom-controls.git', :tag => "2.0.0", :branch => "controls2" }

    s.ios.deployment_target = '10.0'
    s.source_files = "ODControls", "ODControls/**/*.{h,m,swift}"
    s.resources    = 'ODControls/**/*.{xcassets,storyboard,xib}'

    s.dependency 'DZNEmptyDataSet'
    s.dependency 'libPhoneNumber-iOS'

    s.pod_target_xcconfig = { 'SWIFT_VERSION' => '3.2' }
end
