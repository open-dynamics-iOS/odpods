#
# Be sure to run `pod lib lint ODControls.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
    s.name             = 'ODControls'
    s.version          = '1.2.2'
    s.summary          = 'A collection of iOS controls in swift to facilitate creation of forms in iOS'
    s.description      = '1. PickerView text field, 2. DatePicker text field, 3. Multiple Selection textfield'
    s.homepage         = 'http://www.opendynamics.com.my'
    s.license          = 'GNU GPLv3'
    s.author           = { "Farooq Zaman" => "farooq@opendynamics.com.my" }
    s.platform         = :ios, "9.3"
    s.source           = { :git => 'https://farooq_zaman@bitbucket.org/open-dynamics-iOS/custom-controls.git', :tag => "1.2.2", :branch => "mma" }

    s.ios.deployment_target = '10.0'
    s.source_files = "ODControls", "ODControls/**/*.{h,m,swift}"
    s.resources    = 'ODControls/**/*.{xcassets,storyboard,xib}'

    s.dependency 'DZNEmptyDataSet'
    s.dependency 'libPhoneNumber-iOS'
    s.dependency 'RealmSwift'
    s.dependency 'SwiftFetchedResultsController'

    s.pod_target_xcconfig = { 'SWIFT_VERSION' => '3' }
end
