Pod::Spec.new do |s|

  s.name         = "ODRegistration"
  s.version      = "1.1.0"
  s.summary      = "A template library from user registration"
  s.description  = "ODRegistration is a basic framework for user registration. More complex user registrations can be created from this library"
  s.homepage     = "http://www.opendynamics.com.my"
  s.license      = "GNU GPLv3"
  s.author       = { "Farooq Zaman" => "farooq.zaman@me.com" }
  s.platform     = :ios, "10.0"
  s.source       = { :git => "https://farooq_zaman@bitbucket.org/opendynamics/odregistration.git", :tag => "1.1.0", :branch => "mma" }
  s.source_files  = "ODRegistration", "ODRegistration/**/*.{h,m,swift}"
  s.resources = "ODRegistration/**/*.{xcassets,storyboard,xib}"

  s.dependency 'libPhoneNumber-iOS', '~> 0.9.2'
  s.dependency 'KVNProgress'
  s.dependency 'Alamofire', '~> 4.5'
  s.dependency 'AlamofireObjectMapper', '~> 5.0'
  s.dependency 'ODControls', '~> 2.0.1'
  s.dependency 'Toast-Swift', '~> 2.0.0'

  s.pod_target_xcconfig = { 'SWIFT_VERSION' => '3.2' }

end
