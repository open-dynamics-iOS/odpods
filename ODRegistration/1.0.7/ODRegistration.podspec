Pod::Spec.new do |s|

  s.name         = "ODRegistration"
  s.version      = "1.0.7"
  s.summary      = "A template library from user registration"
  s.description  = "ODRegistration is a basic framework for user registration. More complex user registrations can be created from this library"
  s.homepage     = "http://www.opendynamics.com.my"
  s.license      = "GNU GPLv3"
  s.author       = { "Farooq Zaman" => "farooq.zaman@me.com" }
  s.platform     = :ios, "10.0"
  s.source       = { :git => "https://farooq_zaman@bitbucket.org/farooq_zaman/odregistration.git", :tag => "1.0.7", :branch => "msh" }
  s.source_files  = "ODRegistration", "ODRegistration/**/*.{h,m,swift}"
  s.resources = "ODRegistration/**/*.{xcassets,storyboard,xib}"

  s.dependency 'libPhoneNumber-iOS', '~> 0.8'
  s.dependency 'KVNProgress'
  s.dependency 'Alamofire', '~> 4.0'
  s.dependency 'AlamofireObjectMapper', '~> 4.0'
  s.dependency 'ODControls', '~> 1.2.0'
  s.dependency 'Toast-Swift', '~> 2.0.0'

  s.pod_target_xcconfig = { 'SWIFT_VERSION' => '3' }

end
