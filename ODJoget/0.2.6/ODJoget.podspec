#
# Be sure to run `pod lib lint ODJoget.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
    s.name             = 'ODJoget'
    s.version          = '0.2.6'
    s.summary          = 'Generic API to Fetch, store and load Joget objects.'
    s.description      = 'This API provides a generic interface to interact with Joget. It provides three features mainly. 1. Loading objects from joget. 2. Storing objects in realm. 3. Finally retrieving objects from realm'
    s.homepage         = 'http://www.opendynamics.com.my'
    s.license          = 'GNU GPLv3'
    s.author           = { "Farooq Zaman" => "farooq@opendynamics.com.my" }
    s.platform         = :ios, "10.0"
    s.source           = { :git => 'https://bitbucket.org/farooq_zaman/odjoget.git', :tag => "0.2.6" }

    s.ios.deployment_target = '10.0'
    s.source_files = "ODJoget", "ODJoget/**/*.{h,m,swift}"

    s.dependency 'Realm', '~> 3.0.0'
    s.dependency 'RealmSwift', '~> 3.0.0'
    s.dependency 'ObjectMapper'
    s.dependency 'Alamofire'
    s.dependency 'AlamofireObjectMapper'

s.pod_target_xcconfig = { 'SWIFT_VERSION' => '3.2' }
end
